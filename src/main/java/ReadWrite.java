import java.io.*;
import java.util.*;

public class ReadWrite {
    private String fileRead = "src/main/resources/data_sekolah.csv";
    String saveM3 = "src/main/resources/m3.txt";
    String saveFreq = "src/main/resources/freq.txt";

    public List<Integer> baca(){
        try{
            File file = new File(fileRead);
            FileReader read = new FileReader(file);
            BufferedReader br = new BufferedReader(read);

            String line = "";
            String[] arrTemp;
            List<Integer> data = new ArrayList<>();

            while ((line = br.readLine()) != null){
                arrTemp = line.split(";");
                for(int i=1; i<arrTemp.length; i++){
                    String temp = arrTemp[i];
                    Integer bantu = Integer.parseInt(temp);
                    data.add(bantu);
                }
            }
            br.close();
            Collections.sort(data);
            return data;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void m3(){
        try{
            File file = new File(saveM3);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            bwr.write("Berikut Hasil Pengolahan Nilai\n");

            bwr.write("\nBerikut hasil sebaran nilai : ");
            bwr.write("\nMean   : "+mean());
            bwr.write("\nMedian : "+median());
            bwr.write("\nModus  : "+modus());

            bwr.flush();
            bwr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void freq(){
        try{
            File file = new File(saveFreq);
            FileWriter writer = new FileWriter(file);
            BufferedWriter bwr = new BufferedWriter(writer);

            Map<Integer,Integer> read = frekuensi();
            Set<Integer> key = read.keySet();

            bwr.write("Berikut Hasil Pengolahan Nilai\n");
            bwr.write("Nilai \t\t|"+"\t\tFrekuensi\n");
            for(Integer value : key)
                bwr.write(value+"\t\t\t|"+"\t\t\t"+read.get(value)+"\n");

            bwr.flush();
            bwr.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private double mean(){
        List<Integer> list =  baca();
        double sum = 0, temp;
        for(int i=0; i< list.size(); i++){
            temp = list.get(i);
            sum += temp;
        }
        return sum/list.size();
    }

    private double median(){
        double median;
        List<Integer> list = baca();
        int elemen = list.size();
        if(elemen % 2 == 0) {
            double avg = (list.get(elemen/2)+ list.get(elemen/2 -1));
            median = avg / 2;
        }else
            median = list.get(elemen/2);
        return median;
    }

    private int modus(){
        int maxValue = 0, maxCount = 0;
        List<Integer> list = baca();
        for(int i=0; i<list.size(); i++){
            int count = 0;
            for (int j=0; j<list.size(); j++){
                if(list.get(j) == list.get(i)){
                    count++;
                }
            }
            if(count > maxCount){
                maxCount = count;
                maxValue = list.get(i);
            }
        }
        return maxValue;
    }

    private Map<Integer, Integer> frekuensi(){
        List<Integer> list = baca();
        Set<Integer> temp = new HashSet<>(list);
        Map<Integer, Integer> freq = new HashMap<>();

        for (Integer values : temp) {
            freq.put(values, Collections.frequency(list, values));
        }
        return freq;
    }

}
