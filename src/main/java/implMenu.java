import java.util.*;
import static java.lang.System.exit;

public class implMenu implements Menu{
    Scanner in = new Scanner(System.in);
    ReadWrite rw = new ReadWrite();

    @Override
    public void main() {
        System.out.println("--------------------------------------------");
        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
        System.out.println("--------------------------------------------");
        System.out.println("File yg dibaca dari ");
        System.out.println("\nPilih menu : ");
        System.out.println("1. File txt yg menampilkan modus");
        System.out.println("2. File txt yg menampilkan mean-median-modus");
        System.out.println("3. Generate 2 File txt");
        System.out.println("0. Exit");
        toDoMain();
    }

    @Override
    public void first() {
        System.out.println("--------------------------------------------");
        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
        System.out.println("--------------------------------------------");
        System.out.println("File digenerate di "+rw.saveM3);
        rw.m3();
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        toDo2();
    }

    @Override
    public void second() {
        System.out.println("--------------------------------------------");
        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
        System.out.println("--------------------------------------------");
        System.out.println("File digenerate di "+rw.saveFreq);
        rw.freq();
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        toDo2();
    }

    @Override
    public void third() {
        System.out.println("--------------------------------------------");
        System.out.println("APLIKASI PENGOLAH NILAI SISWA");
        System.out.println("--------------------------------------------");
        System.out.println("File digenerate di "+rw.saveM3);
        System.out.println("File digenerate di "+rw.saveFreq);
        rw.m3();
        rw.freq();
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke menu utama");
        toDo2();
    }

    @Override
    public void toDoMain() {
        switch (pilih()){
            case 0:
                exit(0);
                break;
            case 1:
                first();
                break;
            case 2:
                second();
                break;
            case 3:
                third();
                break;
            default:
                out();
                break;
        }
    }

    @Override
    public void toDo2() {
        switch (pilih()){
            case 0:
                exit(0);
                break;
            case 1:
                main();
                break;
            default:
                out();
                break;
        }
    }


    public int pilih(){
        System.out.print("Pilih : ");
        int choose = in.nextInt();
        return choose;
    }

    public void out(){
        System.out.println("MENU YG DIPILIH TIDAK ADA");
        System.out.print("Ketik apasaja dan tekan enter. ");
        String any = in.next();
        main();
    }


}
